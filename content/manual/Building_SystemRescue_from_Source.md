+++
title = "Building SystemRescue from Source"
draft = false
+++

## SystemRescue itself

SystemRescue is based on Arch Linux and uses a patched version of archiso to
build the released .iso images.

You can find all sources, build tools and build documentation at 
https://gitlab.com/systemrescue/systemrescue-sources

## Arch Linux packages

The build process outlined above imports all programs that are not directly developed
as part of SystemRescue from the package repositories of Arch Linux. The source code
and build tooling for all of these programs are available from Arch Linux.

It is suggested to first read the [Arch Build System Documentation](https://wiki.archlinux.org/title/Arch_Build_System).

The build instructions for all packages are stored in PKGBUILD files. These, and eventually patches,
are available for download from https://github.com/archlinux/svntogit-packages

Make sure to use the correct branch and version. If you want to rebuild a package as used in a
specific version of SystemRescue, use the package version that was current at the release date
of SystemRescue. See for example the `BUILD_ID` in `/etc/os-release` inside SystemRescue for this.

When you have the correct PKGBUILD file, run `makepkg` to build. By default this will automatically 
download the required source files from the upstream project. Alternatively Arch Linux publishes 
these sources at `https://archive.archlinux.org/repos/<date>/sources/`. Use the release date 
as noted above.

## systemrescue-custompkg

A few packages that are contained in SystemRescue aren't available in binary form from the
official Arch Linux repositories. These are for example packaged by
[Arch Linux AUR](https://aur.archlinux.org/) or packaged by SytemRescue.

PKGBUILD files, patches and documentation are for these are available at
https://gitlab.com/systemrescue/systemrescue-custompkg
